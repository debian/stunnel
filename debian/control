Source: stunnel4
Section: net
Priority: optional
Build-Depends:
 debhelper (>> 13.15.2~),
 dh-package-notes,
 dh-sequence-single-binary,
 dpkg-build-api (= 1),
 autoconf-archive,
 libssl-dev,
 libsystemd-dev [linux-any],
 libwrap0-dev,
 net-tools [!armel !armhf !hppa !m68k !powerpc !sh4] <!nocheck>,
 netcat-openbsd [!armel !armhf !hppa !m68k !powerpc !sh4] <!nocheck>,
 openssl,
 pkgconf,
 procps [!armel !armhf !hppa !m68k !powerpc !sh4] <!nocheck>,
 python3 [!armel !armhf !hppa !m68k !powerpc !sh4] <!nocheck>,
 python3-cryptography [!armel !armhf !hppa !m68k !powerpc !sh4] <!nocheck>,
Maintainer: Peter Pentchev <roam@debian.org>
Uploaders:
 Laszlo Boszormenyi (GCS) <gcs@debian.org>,
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/debian/stunnel/
Vcs-Git: https://salsa.debian.org/debian/stunnel.git
Homepage: https://www.stunnel.org/
X-DH-Compat: 14
X-Style: black

Package: stunnel4
Architecture: any
Provides:
 stunnel,
Depends:
 netbase,
 openssl,
Pre-Depends:
 adduser,
Suggests:
 logcheck-database,
Description: Universal SSL tunnel for network daemons
 The stunnel program is designed to work  as  SSL  encryption
 wrapper between remote client and local (inetd-startable) or
 remote server. The concept is that having non-SSL aware daemons
 running  on  your  system you can easily setup them to
 communicate with clients over secure SSL channel.
 .
 stunnel can be used to add  SSL  functionality  to  commonly
 used  inetd  daemons  like  POP-2,  POP-3  and  IMAP servers
 without any changes in the programs' code.
 .
 This package contains a wrapper script for compatibility with stunnel 3.x
